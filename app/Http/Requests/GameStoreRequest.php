<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameStoreRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name_game'        => 'required|max:100',
            'url_game'         => 'required',
            'description_game' => 'required',
            'url_image'        => 'required'
        ];
    }

    /**
     * Get messages for validation
     * @return array
     */
    public function messages(){
        return [
            'name_game.required'        => 'Nombre de juego es requerido',
            'name_game.max'             => 'Nombre de juego debe tener un máximo de 100 carácteres',
            'url_game.required'         => 'URL de juego es requerida',
            'description_game.required' => 'Descripción de juego es requerida',
            'url_image.required'        => 'URL de imagen juego es requerida',
        ];
    }
    public function attributes(){
        return [
            'name_game'        => 'nombre juego',
            'url_game'         => 'URL juego',
            'description_game' => 'Descripción juego',
            'url_image'        => 'URL imagen juego'
        ];
    }
}
