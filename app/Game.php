<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{
    protected $primaryKey = 'idGame';
    // setters
    public function setNameGame($name){
        $original_name = $this->name_game;
        if ($original_name != $name)
            $this->name_game = $name;
    }
    public function setUrlGame($url){
        $original_url = $this->url_game;
        if ($original_url != $url)
            $this->url_game = $url;
    }
    public function setDescriptionGame($description){
        $original_description = $this->description_game;
        if ($original_description != $description)
            $this->description_game = $description;
    }
    public function setUrlImage($url_image){
        $original_url_image = $this->url_image;
        if ($original_url_image != $url_image)
            $this->url_image = $url_image;
    }
    public function setStatusGame($status){
        $original_status = $this->status_game;
        if ($original_status != $status)
            $this->status_game = $status;
    }   
}