<?php

namespace App\Http\Middleware;

use App\Game;
use Closure;

class FindGame{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $game = Game::find($request->id);
        if (!$game){
            if ($request->ajax() || $request->is('api/*'))
                return response()->json(['error' => 'Juego no ha sido encontrado'], 404);    
        }
        $request->game = $game;
        
        return $next($request);
    }
}