<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::resource('games', 'GameController');
Route::get('/games',            'GameController@index');
Route::post('/game',            'GameController@store');
Route::get('/game/{id}',        'GameController@show');
Route::put('/game/{id}',        'GameController@update');
Route::delete('/game/{id}',     'GameController@destroy');