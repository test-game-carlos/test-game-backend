<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
#models
use App\Game;
#services
use App\Services\GameServices;
#store request
use App\Http\Requests\GameStoreRequest;
#resource
use App\Http\Resources\Game as GameResource;

class GameController extends Controller{
    protected $gameServices;
    public function __construct(GameServices $gameServices){
        $this->gameServices = $gameServices;
        $this->middleware('findGame')->only('show', 'destroy');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $games = Game::all();
        $resource = $games->map(function ($game){
            return new GameResource($game);
        });
        return response()->json($resource, 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param GameStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GameStoreRequest $request){
        $res = $this->gameServices->create($request);
        if (isset($res->error))
            return response()->json($res, 400);
        return response()->json(200);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show(Request $request){
        $game = $request->game;
        $resource = new GameResource($game);
        return response($resource, 200);
    }

    /**
     * Update the specified resource in storage.
     * @param GameStoreRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GameStoreRequest $request, $id){
        $res = $this->gameServices->update($request, $id);
        if (isset($res->error))
            return response()->json($res, 400);
        return response()->json(200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request){
        $game = $request->game;
        $res = $this->gameServices->delete($game);
        if (isset($res->error))
            return response()->json($res, 400);
        return response()->json(200);
    }
}