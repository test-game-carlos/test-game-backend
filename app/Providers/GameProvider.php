<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GameProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    protected $defer = true;
    public function register()
    {
        $this->app->bind('App\Interfaces\GameInterface', function (){
            return new \GameService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    public function provides(){
        return ['App\Interfaces\GameInterface'];
    }
}
