<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Game extends JsonResource{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        return [
            'idGame' => $this->idGame,
            'name_game' => $this->name_game,
            'url_game'  => $this->url_game,
            'description_game' => $this->description_game,
            'url_image' => $this->url_image,
            'status_game' => $this->status_game,
        ];
    }
}