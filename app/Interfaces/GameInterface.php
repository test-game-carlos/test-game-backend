<?php namespace App\Interfaces;

interface GameInterface{
    public function create($request);
    public function update($request, $id);
    public function delete($game);
}