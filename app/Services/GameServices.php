<?php namespace App\Services;
#models
use App\Game;
#interfaces
use App\Interfaces\GameInterface;

#### ONLY AS A DEMONSTRATION  ####
#### BUSINESS LOGIC PREFERRED HERE ####
class GameServices implements GameInterface{

    /**
     * Create a new game in storage
     * @param $request
     * @return array|object
     */
    public function create($request){
        try{
            $game = new Game();
            $game->setNameGame($request->name_game);
            $game->setUrlGame($request->url_game);
            $game->setDescriptionGame($request->description_game);
            $game->setUrlImage($request->url_image);
            $game->save();
        }
        catch (\Exception $e){
            return $this->_error('save', 'Ha ocurrido un error al intentar guardar.', 400);
        }

        return [];
    }

    /**
     * Update game
     * @param $request
     * @param $game
     * @return array|object
     */
    public function update($request, $id){
        $game = Game::find($id);
        if (!$game)
            return $this->_error('game', 'Juego no ha sido encontrado.', 404);
        try{
            $game->setNameGame($request->name_game);
            $game->setUrlGame($request->url_game);
            $game->setDescriptionGame($request->description_game);
            $game->setUrlImage($request->url_image);
            $game->setStatusGame($request->status_game);
            $game->save();
        }
        catch (\Exception $e){
            return $this->_error('update', 'Ha ocurrido un error al intentar actualizar los datos.', 400);
        }
        return [];
    }

    /**
     * Delete game
     * @param $game
     * @return array|object
     */
    public function delete($game){
        if (!$game)
            return $this->_error('game', 'Juego no ha sido encontrado.', 404);
        try{
            $game->delete();
        }
        catch (\Exception $e){
            return $this->_error('delete', 'Ha ocurrido un error al intentar eliminar el juego.', 400);
        }
        return [];
    }
    
    /**
     * Object for management errors
     * @param $camp
     * @param $msg
     * @param $code
     * @return object
     */
    private function _error($camp, $msg, $code){
        return (object)[
            'error'=>[
                "$camp"=>$msg
            ],
            'code'=>$code
        ];
    }
}